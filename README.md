# GitLab Teacher's Pet

## How it works
This tool leverages GitLab Organizations to create a private "sandbox" for a student to work on their assignments.

At a high level this tool:
- Automates the creation of these organization
- Allows instructor to push a "skeleton" repo to each student organization
- Harvests each students repo onto the instructor's local machine
- When harvesting, the pipeline against "master" is run to make sure tests pass
- The .gitlab.ci file and main test suite file hashes are checked to verify their intregity
- All this harvesting information is reported back in a json file to be easily digested by an instructor

## How to use

1. Update conf file with your API Token, API URL, and usernames for students and instructors
2. Initilize student organizations `python gitlab_teachers_pet.py --action init`
3. Create assignment repo under your "class organization" in GitLab (normal git stuff)
4. Push that code to all student repos `python gitlab_teachers_pet.py --action create --repo <repo_name>`
5. Students write code, until deadline...
6. Collect assignments `python gitlab_teachers_pet.py --action harvest --repo <repo_name>`

When first using commands, try the `--dry_run flag to see what a command will do without actually modifying anything locally, it will still make get requests to GitLab API.