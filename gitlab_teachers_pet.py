import click
import ConfigParser
import json
import gitlab
import subprocess
import hashlib
import time
from collections import deque
import io

def initilize_class_organization(gl, class_name, students, instructors, dry_run):
	"""
	For each student, create an organization <class_name>-<student_name> which will 
	house that student's work for the class
	"""
	print "Creating class organizations for {}".format(class_name)
	for student in students:
		print "Student: {}".format(student)
		student_org_name = "{}-{}".format(class_name, student)
		student_id = None
		student_organization_id = None

		if dry_run:
			print "...dry_run: Creating student org: {} for {} with instructors {}"\
				.format(student_org_name,
						student,
						instructors)
		else:
			# Look up student to get ID
			try:
				student_user = gl.users.list(username=student)[0]
			except IndexError:
				print "...Student {} does not exist".format(student)
				break

			student_id = student_user.id

			# Create organization
			try:
				student_organization = gl.groups.create(
									{
										"name":student_org_name,
										"path":student_org_name
									})
				print "...{} created".format(student_org_name)
				student_organization_id = student_organization.id
			# ...unless it exists
			except gitlab.exceptions.GitlabCreateError:
				print "...Group {} exists, skipping...".format(student_org_name)
				student_organization = gl.groups.search(student_org_name)
				student_organization_id = student_organization[0].id

			# Give each instructor master access to organization
			for instructor in instructors:
				instructor_user_id = gl.users.list(username=instructor)[0].id
				try:
					gl.group_members.create({"user_id": instructor_user_id,
											 "access_level": gitlab.MASTER_ACCESS},
											 group_id=student_organization_id)
					print "...Instructor {} added to {}".format(instructor, student_org_name)
				# unless they're already there
				except gitlab.exceptions.GitlabCreateError:
					print "...Instructor {} already a member of repo {}".format(instructor, student_org_name)


			#Give student owner access
			try:
				gl.group_members.create({"user_id": student_id,
													 "access_level": gitlab.OWNER_ACCESS},
													 group_id=student_organization_id)
				print "...Student {} {} added as owner of {}".format(student, student_id, student_org_name)
			# unless they're already there
			except gitlab.exceptions.GitlabCreateError:
				print "...Student {} already a member of repo {}".format(student, student_org_name)	

def create_assignment_repositories(gl, class_name, repo, students, dry_run):
	# Given a class and repo assignment (gitlab.cs.wfu.edu/<class_name>/<repo>)
	# Create a repo within each student <class_name>-<student_name> organization with the files from repo assignment
	print "Creating Assignment Repos for Class: {}, Assignment {}".format(class_name, repo)
	assignment_repo = gl.projects.get("{}/{}".format(class_name, repo))

	src_repo_dir="repos/src/{}".format(repo)


	current_user = gl.user

	# Clone down assignment repo
	print "Cloning assignment repo {}/{}".format(class_name, repo)
	clone_assignment = "git clone http://{}@gitlab.cs.wfu.edu/{}/{}.git {}".format(current_user.username, class_name, repo, src_repo_dir)
	process=subprocess.Popen(clone_assignment.split(), stdout=subprocess.PIPE)
	output,error = process.communicate()


	for student in students:
		student_org_name = "{}-{}".format(class_name, student)
		student_id = None
		student_organization_id = None
		student_repo_dir = "repos/tmp/{}".format(student)

		# Get student ID
		try:
			student_id = gl.users.list(username=student)[0].id
		except IndexError:
			print "...Student {} does not exist".format(student)
			break

		# Get org ID
		try:
			student_organization_id = gl.groups.search(student_org_name)[0].id
		except IndexError:
			print "...Student Organization {} does not exist.".format(student_org_name)
			break

		if dry_run:
			print "...dry_run: Creating repo {} in {} for {}".format(assignment_repo.path, student_organization_id, student)

		else:
			# Create assignment repo
			try:
				student_assignment_repo = gl.projects.create({"name":repo,
															  "namespace_id": student_organization_id})
				student_assignment_id = student_assignment_repo.id
			except gitlab.exceptions.GitlabCreateError:
				print "...Repo {} exists for student {}".format(repo, student)
				student_assignment_id = gl.projects.get("{}/{}".format(student_org_name, repo)).id
				student_assignment_repo = gl.projects.get("{}/{}".format(student_org_name, repo))
			
			# Clone assignment repo
			clone_student_assignment = "git clone http://{}@gitlab.cs.wfu.edu/{}/{}.git {}".format(current_user.username, student_org_name, repo, student_repo_dir)
			process=subprocess.Popen(clone_student_assignment.split(), stdout=subprocess.PIPE)
			output,error = process.communicate()

			# Backup student .git
			print "Copying files from {} to {}".format(src_repo_dir, student_repo_dir)
			backup_cmd = "mv {}/.git {}/.git.bak".format(student_repo_dir, student_repo_dir)
			process=subprocess.Popen(backup_cmd.split(), stdout=subprocess.PIPE)
			output,error = process.communicate()

			# Copy src files to student .git
			cp_cmd = "cp {}/* {}/".format(src_repo_dir, student_repo_dir)
			process=subprocess.Popen(cp_cmd, stdout=subprocess.PIPE, shell=True)
			output,error = process.communicate()

			cp_pipeline_cmd = "cp {}/.gitlab-ci.yml {}/".format(src_repo_dir, student_repo_dir)
			process=subprocess.Popen(cp_pipeline_cmd, stdout=subprocess.PIPE, shell=True)
			output,error = process.communicate()

			# Restore student .git
			backup_cmd = "mv {}/.git.bak {}/.git".format(student_repo_dir, student_repo_dir)
			process=subprocess.Popen(backup_cmd.split(), stdout=subprocess.PIPE)
			output,error = process.communicate()

			# Add and commit all files
			git_add = "git --git-dir={}/.git/ --work-tree={}/ add .".format(student_repo_dir, student_repo_dir)
			git_commit = """git --git-dir={}/.git/ --work-tree={}/ commit -m 'Initial commit from teachers_pet' """.format(student_repo_dir, student_repo_dir)
			git_push = "git --git-dir={}/.git/ --work-tree={}/ push".format(student_repo_dir, student_repo_dir)

			process=subprocess.Popen(git_add, stdout=subprocess.PIPE, shell=True)
			output,error = process.communicate()

			process=subprocess.Popen(git_commit, stdout=subprocess.PIPE, shell=True)
			output,error = process.communicate()

			process=subprocess.Popen(git_push, stdout=subprocess.PIPE, shell=True)
			output,error = process.communicate()

			# Change default branch to `develop`
			print "Adding branch and changing default to dev for {}".format(student)

			git_branch="git --git-dir={}/.git/ --work-tree={}/ checkout -b develop".format(student_repo_dir, student_repo_dir)
			git_branch_push="git --git-dir={}/.git/ --work-tree={}/ push --set-upstream origin develop".format(student_repo_dir, student_repo_dir)

			process=subprocess.Popen(git_branch, stdout=subprocess.PIPE, shell=True)
			output,error = process.communicate()

			process=subprocess.Popen(git_branch_push, stdout=subprocess.PIPE, shell=True)
			output,error = process.communicate()

			# Change default branch
			student_assignment_repo.default_branch = "develop"
			student_assignment_repo.save()

			# Clean up
			print "Deleting tmp repo {}".format(student_repo_dir)
			rm_tmp = "rm -rf {}".format(student_repo_dir)
			process=subprocess.Popen(rm_tmp, stdout=subprocess.PIPE, shell=True)
			output,error = process.communicate()

def run_master_pipelines(gl, class_name, repo, students, dry_run):
		# Run pipeline against master
		# ACHTUNG! The skeleton repo should PASS v Develop, but FAIL for master, this insures that a student passing master means they've done
		# SOMETHING to the code
	print "Starting pipelines"
	for student in students:
		student_org_name = "{}-{}".format(class_name, student)

		# Reset to none in case of error on previous student
		student_id = None
		student_organization_id = None
		pipelines = None
		student_assignment_id = None
		student_assignment_repo = None

		# Get ID and repo handle
		try:
			student_assignment_repo = gl.projects.get("{}/{}".format(student_org_name, repo))
			student_organization_id = gl.groups.search(student_org_name)[0].id
			student_assignment_id = student_assignment_repo.id
		except gitlab.exceptions.GitlabGetError:
			print "... pipelines: unable to locate assignment or organization for {}, attempted to find {}/{}"\
				.format(student, student_org_name, repo)
			break

		if dry_run:
			print "...dry_run: Start new pipeline for {}, Repo: {}".format(student, student_assignment_id)
			return None
		else:
			new_master_pipeline = gl.project_pipelines.create({'project_id': student_assignment_id, "ref": "master"})
		print "...started new pipeline for {}, Repo: {}".format(student, student_assignment_id)

	print "All pipelines started"

def harvest_master_pipelines(gl, class_name, repo, students, dry_run):
	print "Starting attempt to harvest pipelines"
	queue = deque()
	completed = []

	for student in students:
		student_org_name = "{}-{}".format(class_name, student)

		# Reset to none in case of error on previous student
		student_id = None
		student_organization_id = None
		pipelines = None
		student_assignment_id = None
		student_assignment_repo = None

		# Get ID and repo handle
		try:
			student_assignment_repo = gl.projects.get("{}/{}".format(student_org_name, repo))
			student_organization_id = gl.groups.search(student_org_name)[0].id
			student_assignment_id = student_assignment_repo.id
		except gitlab.exceptions.GitlabGetError:
			print "... pipelines: unable to locate assignment or organization for {}, attempted to find {}/{}"\
				.format(student, student_org_name, repo)
			break
		if dry_run:
			"...dry_run:Check for pipeline running for {} {}".format(student_assignment_id, student_org_name)
		else:
			mru_pipeline = gl.project_pipelines.list(project_id=student_assignment_id)[0]

			if mru_pipeline.status != 'failed' and mru_pipeline.status != 'success':
				queue.append((mru_pipeline.id, student_assignment_id, False))
	if dry_run:
		return None

	# Doing this in a queue to not just spam the api
	queue.append("Block")
	while queue:
		curr = queue.popleft()
		if curr == "Block":
			if len(queue) == 0:
				break
			print "...{} remaining pipelines running".format(len(queue))
			print("...End of full loop of queue, sleeping...")
			for i in range(5):
				time.sleep(2)
				print(".")
			queue.append("Block")
		else:
			mru_pipeline = gl.project_pipelines.get(curr[0], project_id=curr[1])
			if mru_pipeline.status == 'failed' or mru_pipeline.status == 'success':
				completed.append((curr[1], mru_pipeline.status))
			else:
				queue.append((curr[0], curr[1], False))

	print "All pipelines completed"
	return completed


def harvest_repositories(gl, class_name, repo, students, full_test_file, pipeline_file, dry_run):
	# Given a repo name
	# For each student
		# Clone each student's assignment repo into directory
		# For each assignment, take the most recent pipeline, and run it against master
		# Check file hash of src/gitlab-ci and src/test_full against student's, include message in log if different

	print "Harvesting Repos for Class: {}, Assignment {}".format(class_name, repo)
	assignment_repo = gl.projects.get("{}/{}".format(class_name, repo))

	src_repo_dir="repos/src/{}".format(repo)

	current_user = gl.user

	hash_mismatch = []
	completed = []

	# Clone down assignment repo
	print "...Updating assignment repo {}/{}".format(class_name, repo)
	if dry_run:
		pass
	else:
		clone_assignment = "git --git-dir={}/.git/ --work-tree={}/ pull".format(src_repo_dir, src_repo_dir)
		process=subprocess.Popen(clone_assignment.split(), stdout=subprocess.PIPE)
		output,error = process.communicate()

		# Compute file hash of full_test and pipeline file
		fnamelist_master = [src_repo_dir+"/"+full_test_file, src_repo_dir + "/" + pipeline_file]

		master_hash_res = [(fname, hashlib.sha256(open(fname, 'rb').read()).digest()) for fname in fnamelist_master]

	for student in students:
		student_org_name = "{}-{}".format(class_name, student)
		student_repo_dir = "repos/student/{}/{}".format(student, repo)

		# Reset to none in case of error on previous student
		student_id = None
		student_organization_id = None
		pipelines = None
		student_assignment_id = None
		student_assignment_repo = None

		print "...Harvesting {}".format(student)

		# Get ID and repo handle
		try:
			student_assignment_repo = gl.projects.get("{}/{}".format(student_org_name, repo))
			student_organization_id = gl.groups.search(student_org_name)[0].id
			student_assignment_id = student_assignment_repo.id
		except gitlab.exceptions.GitlabGetError:
			print "... repos: unable to locate assignment or organization for {}, attempted to find {}/{}"\
				.format(student, student_org_name, repo)
			break

		if dry_run:
			print "...dry_run:clone repo and compute file hashes {}/{} {}".format(student_org_name, repo, student_repo_dir)
			return None, None
		else:

			# Clone repo
		 	clone_student_assignment = "git clone http://{}@gitlab.cs.wfu.edu/{}/{}.git {}".format(current_user.username, student_org_name, repo, student_repo_dir)
		 	process=subprocess.Popen(clone_student_assignment.split(), stdout=subprocess.PIPE)
			output,error = process.communicate()

			# Compute file hashes for student assignment and check them
			fnamelst_test = [student_repo_dir+"/"+full_test_file, student_repo_dir+"/"+pipeline_file]
			test_hash_res =[(fname, hashlib.sha256(open(fname, 'rb').read()).digest()) for fname in fnamelst_test]

			for idx in range(len(master_hash_res)):
				if test_hash_res[idx][1] != master_hash_res[idx][1]:
					print "...WARN: Student {} hash does not match for {}".format(student, master_hash_res[idx][0])
					hash_mismatch.append((student, student_assignment_id, test_hash_res[idx][0]))
			completed.append((student, student_assignment_id, student_repo_dir))

	print "...All student repos cloned, see ./repos/student"
	return hash_mismatch, completed

def harvest_results(completed_clones, completed_pipelines, hash_mismatch, dry_run):
	"""
	clones = (student_name, assignment_id, path)
	pipelines = (assignment_id, failed)
	hash = (student_name, assignment_id, issue_file)

	result_dict = {ID:{name, path, hash_issue:[], pipeline_status}}
	"""
	if dry_run:
		return {"error":"False", "message":"dry run"}
	result_dict = {}
	for clone in completed_clones:
		result_dict[clone[1]] = {"username":clone[0], "path":clone[2], "hash_issue":[]}
	for pipeline in completed_pipelines:
		result_dict[pipeline[0]]["pipeline_status"] = pipeline[1]
	for h in hash_mismatch:
		result_dict[h[1]]["hash_issue"].append(h[2])

	final_dict = {}
	for key in result_dict:
		final_dict[result_dict[key]["username"]] = {"repo_id":key, "path":result_dict[key]["path"],
												"hash_issue":result_dict[key]["hash_issue"],
												"pipeline_status":result_dict[key]["pipeline_status"]}
	return final_dict


@click.command()

@click.option("--config_file", default="gitlab_teachers_pet_conf.conf", help="path to config file")
@click.option("--repo", help="GitLab repository name")
@click.option("--action", type=click.Choice(["init", "create", "harvest"]), help="Action to take", required=True)
@click.option("--dry_run", is_flag=True)
def main(config_file, repo, action, dry_run):

	# Read Config Details
	config = ConfigParser.ConfigParser()
	config.read(config_file)

	student_list = json.loads(config.get("people","students"))
	instructor_list = json.loads(config.get("people", "instructors"))
	my_token = config.get("credentials", "api_token")
	class_group_name = config.get("gitlab", "class_name")
	full_test_file = config.get("gitlab", "test_file")
	pipeline_file = config.get("gitlab", "pipeline_file")
	output_file = config.get("gitlab", "output_file_ending")
	api_url = config.get("gitlab", "api_url")

	# Quick Config validation
	if student_list == [] or instructor_list == []:
		print("Error: Require some students or instructors, given: Students: {}, Instructors: {}"
			.format(student_list, instructor_list))
		return 1

	# Set up GitLab connection
	gl = gitlab.Gitlab("http://gitlab.cs.wfu.edu", my_token)
	gl.auth()

	if action == "init":
		initilize_class_organization(gl, class_group_name, student_list, instructor_list, dry_run)

	if action == "create":
		create_assignment_repositories(gl, class_group_name, repo, student_list, dry_run)

	if action == "harvest":
		output_file = repo + output_file
		run_master_pipelines(gl, class_group_name, repo, student_list, dry_run)
		hash_mismatch, completed_clones = harvest_repositories(gl, class_group_name, repo, student_list, full_test_file, pipeline_file, dry_run)
		completed_pipelines = harvest_master_pipelines(gl, class_group_name, repo, student_list, dry_run)
		result_dict = harvest_results(completed_clones, completed_pipelines, hash_mismatch, dry_run)
		print json.dumps(result_dict)
		with open(output_file, 'w') as outfile:
			json.dump(result_dict, outfile, sort_keys = True, indent = 4,
	               ensure_ascii = False)

if __name__ == '__main__':
	main()